---
name: "Desktop PC"
comment: "experimental"
deviceType: "tv"
portType: "Native"
image: "https://unity8.io/img.png"
maturity: .1
disableBuyLink: true
installLink: "https://unity8.io"
description: "You can install Lomiri, the desktop environment of Ubuntu Touch previously known as Unity 8, on all debian-derived Linux distributions, including Ubuntu."
---
